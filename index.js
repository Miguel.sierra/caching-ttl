var express = require("express");
const bodyParser = require("body-parser");
const myCache = require("./cache");
const fibonnaci = require("./function");
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));

const checkCache = require("./middleware");

app.get("/:id", checkCache, async function(req, res) {
  try {
    let { id } = req.params;
    let result = await fibonnaci(30);
    myCache.set(id, result, 10000);
    res.send("Hello World! \n No cache: " + result);
  } catch (error) {
    console.log(error);
  }
});

app.listen(3000, function() {
  console.log("Example app listening on port 3000!");
});
