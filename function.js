function fibonnaci(n) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      var fib = [0, 1];
      for (var i = fib.length; i < n; i++) {
        fib[i] = fib[i - 2] + fib[i - 1];
      }
      resolve(fib);
    }, 5000);
  });
}

module.exports = fibonnaci;
