const NodeCache = require("node-cache");
const myCache = new NodeCache({ stdTTLL: 0, checkperiod: 86400 });

//el stdTTLL es para decir que en memoria el elemento generado de cache va a persistir
//el checkperiod es para decirle en segundos cuando va a borrar el cache 0 para ilimitado

module.exports = myCache;
