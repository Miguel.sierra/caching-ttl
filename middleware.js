const myCache = require("./cache");

const checkCache = async (req, res, next) => {
  try {
    const { id } = req.params;
    if (myCache.has(id)) {
      res
        .status(200)
        .send(`Hello World! \n Cache response: ${myCache.get(id)}`);
    } else {
      next();
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = checkCache;
